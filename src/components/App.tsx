import * as React from "react";
import AstronautsTable from "./Table";

class App extends React.Component<{}, {}> {
    public render() {
        return (
            <div>
                <AstronautsTable/>
            </div>
        );
    }
}

export default App;
