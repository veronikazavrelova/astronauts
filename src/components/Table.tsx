import * as React from "react";
import {Table, Popconfirm, Tag, Divider, Button} from "antd";
import styled from "styled-components";
import {addAstronaut, IAstronaut, editAstronaut, getAstronauts, removeAstronaut} from "../data/astronauts";
import compareAlphabet from "../functions/compareAlphabet";
import AstronautForm from "./AstronautForm";

const StyledContainer = styled.div`
    width: 90%;
    margin: auto;
    white-space: nowrap;
    margin-top: 50px;
`;

const StyledButtonWrap = styled.div`
    margin-bottom: 20px;
`;

const StyledTableWrap = styled.div`
    overflow: auto;
`;

interface IAstronautsTableState {
    columns;
    showCreateAstronautForm: boolean;
    showEditableAstronautForm: boolean;
    editableAstronaut: IAstronaut;
}

class AstronautsTable extends React.Component <{}, IAstronautsTableState> {

    constructor(props) {
        super(props);
        this.state = {
            editableAstronaut: null,
            showCreateAstronautForm: false,
            showEditableAstronautForm: false,
            columns: [{
                title: "příjmení",
                dataIndex: "lastName",
                key: "lastName",
                minWidth: "100px",
                sorter: (a, b) => compareAlphabet(a.lastName, b.lastName),
            }, {
                title: "křestní jméno",
                dataIndex: "firstName",
                key: "firstName",
                minWidth: "100px",
                sorter: (a, b) => compareAlphabet(a.firstName, b.firstName),
            }, {
                title: "datum narození",
                dataIndex: "dateOfBirth",
                key: "dateOfBirth",
                minWidth: "100px",
            }, {
                title: "superschopnost",
                key: "superPowers",
                dataIndex: "superPowers",
                minWidth: "200px",
                editable: true,
                render: (tags) => (
                    <span>
                        {tags.map((tag) => <Tag color="blue" key={tag}>{tag}</Tag>)}
                    </span>
                ),
            }, {
                title: "Akce",
                key: "action",
                width: 150,
                render: (text, record) => {
                    return (
                        <div>
                            <a
                                onClick={() => {
                                    this.setState({
                                        showEditableAstronautForm: true,
                                        editableAstronaut: record,
                                    });
                                }}
                            >
                                Upravit
                            </a>
                            <Divider type="vertical" />
                            <Popconfirm
                                title="Odstránit položku?"
                                onConfirm={() => this.handleDelete(record.key)}
                                okText="Ano"
                                cancelText="Zrušit"
                            >
                                <a>Vymazat</a>
                            </Popconfirm>
                        </div>
                    );
                },
            }],
        };
    }

    public render() {

        return (
            <StyledContainer>
                <StyledButtonWrap>
                    <Button
                        type="primary"
                        onClick={() => {
                            this.setState({showCreateAstronautForm: true});
                        }}
                    >
                        Přidat astronauta
                    </Button>
                </StyledButtonWrap>
                <StyledTableWrap>
                    <Table
                        dataSource={getAstronauts()}
                        pagination={{pageSize: 10}}
                        columns={this.state.columns}
                    />
                </StyledTableWrap>
                {/*podmienka zobrazenie formulara na pridanie astronauta*/}
                <AstronautForm
                    modalTitle={"Přidat astronauta"}
                    visible={this.state.showCreateAstronautForm}
                    onOk={this.handleCreate}
                    onCancel={() => {
                        this.setState({showCreateAstronautForm: false});
                    }}
                />
                {/*podmienka zobrazenie formulara na editovanie astronauta*/}
                <AstronautForm
                    modalTitle={"Upravit astronauta"}
                    visible={this.state.showEditableAstronautForm}
                    editableAstronaut={this.state.editableAstronaut}
                    onOk={this.handleEdit}
                    onCancel={() => {
                        this.setState({showEditableAstronautForm: false});
                    }}
                />
            </StyledContainer>
        );
    }

    public drawTable = () => {
        this.forceUpdate();

    }
    public handleDelete = (key) => {
        removeAstronaut(key);
        this.drawTable();
    }
    public handleCreate = (values) => {
        addAstronaut(values);
        this.setState({
            showCreateAstronautForm: false,
        });
    }
    public handleEdit = (values) => {
        editAstronaut(this.state.editableAstronaut.key, values);
        this.setState({
            showEditableAstronautForm: false,
            editableAstronaut: null,
        });
    }

}

export default AstronautsTable;
