import * as React from "react";
import {Modal, Form, Input, Icon, DatePicker, Select} from "antd";
import {FormComponentProps} from "antd/es/form";
import {IAstronaut, FORMAT_DATE} from "../data/astronauts";
import * as moment from "moment";

const FormItem = Form.Item;

interface IAstronautFormProps extends FormComponentProps {
    visible: boolean;
    onOk: (values) => void;
    onCancel: () => void;
    editableAstronaut?: IAstronaut;
    modalTitle: string;
}

const AstronautForm = Form.create()(
    class extends React.Component  <IAstronautFormProps, {}> {

        public render() {
            const {visible, form, modalTitle, editableAstronaut} = this.props;
            const { getFieldDecorator } = form;
            return (
                <Modal
                    visible={visible}
                    title={modalTitle}
                    okText="Uložit"
                    cancelText="Zpět"
                    onCancel={this.handleCancel}
                    onOk={this.handleOk}
                >
                    <Form layout="vertical">
                        {/*input pre priezvisko astronauta*/}
                        <FormItem label="Příjmění">
                            {getFieldDecorator("lastName", {
                                rules: [{ required: true, message: "Prosím napište příjmení astronauta!" }],
                                initialValue: editableAstronaut ? editableAstronaut.lastName : null,
                            })(
                                <Input
                                    placeholder={"příjmění astronauta"}
                                    prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }}/>}
                                />,
                            )}
                        </FormItem>
                        {/*input pre meno astronauta*/}
                        <FormItem label="Jméno">
                            {getFieldDecorator("firstName", {
                                rules: [{ required: true, message: "Prosím napište jméno astronauta!" }],
                                initialValue: editableAstronaut ? editableAstronaut.firstName : null,
                            })(
                                <Input
                                    placeholder={"jméno astronauta"}
                                    prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }}/>}
                                />,
                            )}
                        </FormItem>
                        {/*datepicker pre vyber datumu narodenia */}
                        <FormItem label="Datum narození">
                            {getFieldDecorator("dateOfBirth", {
                                rules: [{ type: "object", required: true, message: "Prosím vyberte datum narození astronauta!" }],
                                initialValue: editableAstronaut ? moment(editableAstronaut.dateOfBirth, FORMAT_DATE) : null,
                            })(
                                <DatePicker format={FORMAT_DATE} placeholder={"datum narození"}/>,
                            )}
                        </FormItem>
                        {/*selekt pre vkladanie superschopnosti*/}
                        <FormItem label="Superschopnosti">
                            {getFieldDecorator("superPowers", {
                                initialValue: editableAstronaut ? editableAstronaut.superPowers : [],
                            })(
                                <Select
                                    mode="tags"
                                    style={{ width: "100%" }}
                                    placeholder="Zadejte superschopnosti"
                                />,
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            );
        }

        public handleCancel = () => {
            this.props.onCancel();
        }

        public handleOk = () => {
            this.props.form.validateFields((err, values) => {
                if (err) {
                    return;
                }
                values.dateOfBirth = values.dateOfBirth.format(FORMAT_DATE);
                this.props.form.resetFields();
                this.props.onOk(values);
            });
        }
     },
);

export default AstronautForm;
