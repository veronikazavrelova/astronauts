export interface IAstronaut extends IAstronautInput {
    key: number;
}

export interface IAstronautInput {
    lastName: string;
    firstName: string;
    dateOfBirth: string;
    superPowers: string[];
}

let astronauts: IAstronaut[] = [
    {
        key: 0,
        lastName: "Brown",
        firstName: "John",
        dateOfBirth: "04. 05. 1989",
        superPowers: ["neviditelnost", "telekineze"],
    }, {
        key: 1,
        lastName: "Green",
        firstName: "Jim",
        dateOfBirth: "07. 02. 1975",
        superPowers: ["super rychlost", "ovládání času"],
    }, {
        key: 2,
        lastName: "Black",
        firstName: "Joe",
        dateOfBirth: "11. 08. 1991",
        superPowers: ["léčitelství", "čtení myšlenek", "teleportace"],
    },
];

let keyNumber: number = astronauts.length;

export const FORMAT_DATE = "DD. MM. YYYY";

export const getAstronauts = (): IAstronaut[] => {
    return astronauts;
};

export const addAstronaut = (values: IAstronautInput): IAstronaut =>  {
    const astronaut: IAstronaut = {
        key: keyNumber++,
        lastName: values.lastName,
        firstName: values.firstName,
        dateOfBirth: values.dateOfBirth,
        superPowers: values.superPowers,
    };
    astronauts.push(astronaut);
    return astronaut;
};

export const removeAstronaut = (key: number): void => {
    astronauts = astronauts.filter((astronaut) => astronaut.key !== key);
};

export const editAstronaut = (key: number, values: IAstronautInput) => {
    const editableAstronaut = astronauts.filter((astronaut) => astronaut.key === key)[0];
    editableAstronaut.lastName = values.lastName;
    editableAstronaut.firstName = values.firstName;
    editableAstronaut.dateOfBirth = values.dateOfBirth;
    editableAstronaut.superPowers = values.superPowers;
};
